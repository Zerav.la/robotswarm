# RobotSwarm



## Instalación en WSL

1. Instalar Ubuntu 20.04 desde la tienda de MS Store (versiones posteriores, ROS Noetic no es soportado).
2. Reiniciar PC 
3. Abrir Terminal 
4. Crear usuario y poner contraseña (Si no sale automáticamente, escribir comando wsl) 
5. Cerrar ventana de Ubuntu 
6. Abrir una nueva ventana del terminal 
7. Escribir comando: 
   `` cd ~/ ``
8. Ingresar los siguientes comandos: 
    ```
    sudo apt update 
    sudo apt upgrade 
    
    ```

9. Ingresar los siguientes comandos para la instalación de ROS
    ```
    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo  apt-key add - 
    sudo apt update 
    sudo apt install ros-noetic-desktop-full 
    source /opt/ros/noetic/setup.bash 
    echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc 
    source ~/.bashrc 
    sudo apt install python3-rosdep python3-rosinstall python3-rosinstall-generator  python3-wstool build-essential 
    sudo apt install python3-rosdep 
    sudo rosdep init 
    rosdep update     
    
    ```
10. Descargar el repositorio en ``~/``
11. Modificar _basrc_ con ```nano ~/.basrc`` y agregar esto al final del archivo:
    ```
    source /opt/ros/noetic/setup.bash
    source ~/RobotSwarm/swarm_ws/devel/setup.bash
    export DISPLAY="$(grep nameserver /etc/resolv.conf | sed 's/nameserver //'):0"
    export GAZEBO_IP=127.0.0.1
    export LIBGL_ALWAYS_INDIRECT=0
    ```

12. Cambiar WSL a versión 1 con ``wsl --set-version Ubuntu 1`` o ``wsl --set-version Ubuntu-20.04 1``
13. Descargar e instalar el siguiente software VcXsrv (dejar todas las opciones por  defecto en la instalación). Le va a pedir permisos de firewall (permitirlos)

![Semantic description of image](/images/1.png)
![Semantic description of image](/images/2.png)
![Semantic description of image](/images/3.png)

14. Clic en "Save configuration", y guardenlo en el escritorio
15. En el explorador de archivos de Windows, dirigirse a la siguiente carpeta: 
    ``C:\Users(o Usuarios en español)\<Tu usuario>\AppData\Local\Packages\ ``
16. Dentro de esa carpeta, buscar una carpeta con un nombre similar a este: 
    ``CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc``
17. Mover el archivo ``config.xlaunch`` (dejado en el escritorio) a ``LocalState\rootfs\home\<tu nombre de usuario en linux>/RobotSwarm``

18. Seguir el siguiente procedimiento: 

    a. Hay que asegurar que estamos en la carpeta home con cd ~/ 

    b. Entrar a la carpeta RobotSwarm con cd RobotSwarm

    c. Comando touch display.sh 

    d. Comando nano display.sh (se debe abrir GNU nano) 

    e. Escribir powershell.exe Start RobotSwarm/config.xlaunch 

    f. Cerrar con Ctrl+X 

    g. Guardar con Y 

    h. Guardar nombre del archivo con ENTER

    i. Usar el comando ``source RobotSwarm/display.sh``

19. Convertir WSL a versión 2

Prueba: 

Escriba el comando ``gazebo``

Si no funciona. Haga lo siguiente: 

    a. Corra el comando (una sola línea): ``sudo apt install --reinstall  libqt5widgets5 libqt5gui5 libqt5dbus5 libqt5network5 libqt5core5a`` 

    b. Corra el comando (una sola línea): sudo strip --remove-section=.note.ABI tag /usr/lib/x86_64-linux-gnu/libQt5Core.so.5

    c. Si este último da error, corra el comando (una sola línea): sudo strip -- remove-section=.note.ABI-tag /usr/lib64/libQt5Core.so.5

    d. Vuelva a correr gazebo 
    
Cerrar gazebo (cerrar manualmente en la X o pulsar Ctrl+C en la terminal)


## Nativamente en Ubuntu 20.04 (no funciona en versiones posteriores)

1. Correr este comando: ``wget -c https://raw.githubusercontent.com/qboticslabs/ros_install_noetic/master/ros_install_noetic.sh && chmod +x ./ros_install_noetic.sh && ./ros_install_noetic.sh``

2. Elegir la primera opción _Desktop-full-install_

3. hacer git pull al repositorio

